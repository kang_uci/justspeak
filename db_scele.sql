/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.6.20 : Database - db_scele
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_scele` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_scele`;

/*Table structure for table `tbl_admin` */

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `username` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_admin` */

/*Table structure for table `tbl_bobot` */

DROP TABLE IF EXISTS `tbl_bobot`;

CREATE TABLE `tbl_bobot` (
  `bobot` varchar(5) COLLATE latin1_general_ci NOT NULL,
  `nilai` char(5) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*Data for the table `tbl_bobot` */

/*Table structure for table `tbl_dosen` */

DROP TABLE IF EXISTS `tbl_dosen`;

CREATE TABLE `tbl_dosen` (
  `nidn` varchar(10) NOT NULL,
  `nama_dosen` varchar(30) DEFAULT NULL,
  `jk_dosen` char(8) DEFAULT NULL,
  `alamat_dosen` varchar(50) DEFAULT NULL,
  `hp_dosen` char(15) DEFAULT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_dosen` */

insert  into `tbl_dosen`(`nidn`,`nama_dosen`,`jk_dosen`,`alamat_dosen`,`hp_dosen`) values 
('01012008','Agus Mulyanto. S.Kom','Wanita','Jl. Rajabasa Kp. Keramat No.35 Gg. Ratu','0821 3345 6690'),
('01092010','Sunah Saputri','Wanita','Jl. Rajabasa Kp. Keramat No.35','0821 3345 6625'),
('02102012','Jupriyasi, M.Kom.','Pria','Jl. Pramuka','0821 3345 5565');

/*Table structure for table `tbl_hasil_tes` */

DROP TABLE IF EXISTS `tbl_hasil_tes`;

CREATE TABLE `tbl_hasil_tes` (
  `kode` smallint(3) NOT NULL AUTO_INCREMENT,
  `jb_tes` enum('A','B','C','D') DEFAULT NULL,
  `no_soal` char(3) DEFAULT NULL,
  `no_peserta` char(10) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_hasil_tes` */

/*Table structure for table `tbl_jadwal` */

DROP TABLE IF EXISTS `tbl_jadwal`;

CREATE TABLE `tbl_jadwal` (
  `kd_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `kd_mk` varchar(20) NOT NULL,
  `kd_dosen` varchar(5) NOT NULL,
  `kd_tahun` varchar(20) NOT NULL,
  `jadwal` varchar(100) NOT NULL,
  `kapasitas` int(3) NOT NULL,
  `kelas_program` varchar(10) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  PRIMARY KEY (`kd_jadwal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jadwal` */

/*Table structure for table `tbl_jurusan` */

DROP TABLE IF EXISTS `tbl_jurusan`;

CREATE TABLE `tbl_jurusan` (
  `kd_jurusan` varchar(4) NOT NULL,
  `nama_jurusan` varchar(30) NOT NULL,
  PRIMARY KEY (`kd_jurusan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jurusan` */

insert  into `tbl_jurusan`(`kd_jurusan`,`nama_jurusan`) values 
('RPL','REKAYASA PERANGKAT LUNAK'),
('TKJ','TEKNIK KOMPUTER JARINGAN');

/*Table structure for table `tbl_mhs` */

DROP TABLE IF EXISTS `tbl_mhs`;

CREATE TABLE `tbl_mhs` (
  `nim` char(20) NOT NULL,
  `nama_mhs` varchar(50) NOT NULL,
  `jk` enum('L','P') NOT NULL DEFAULT 'L',
  `agama` varchar(20) DEFAULT NULL,
  `alamat_mhs` varchar(50) NOT NULL,
  `no_hp` char(15) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mhs` */

/*Table structure for table `tbl_mk` */

DROP TABLE IF EXISTS `tbl_mk`;

CREATE TABLE `tbl_mk` (
  `kd_mk` varchar(10) NOT NULL DEFAULT '',
  `nama_mk` varchar(100) DEFAULT NULL,
  `jum_sks` int(2) DEFAULT NULL,
  `semester` int(2) DEFAULT NULL,
  `prasyarat_mk` varchar(50) DEFAULT NULL,
  `kode_jur` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`kd_mk`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_mk` */

/*Table structure for table `tbl_nilai` */

DROP TABLE IF EXISTS `tbl_nilai`;

CREATE TABLE `tbl_nilai` (
  `nim` varchar(20) NOT NULL,
  `kd_mk` varchar(50) NOT NULL,
  `kd_dosen` varchar(20) NOT NULL,
  `kd_tahun` varchar(20) NOT NULL,
  `semester_ditempuh` int(2) NOT NULL,
  `grade` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_nilai` */

/*Table structure for table `tbl_peserta` */

DROP TABLE IF EXISTS `tbl_peserta`;

CREATE TABLE `tbl_peserta` (
  `no_peserta` char(10) NOT NULL,
  `nama_peserta` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `ttl` varchar(50) DEFAULT NULL,
  `jk_peserta` enum('L','P') DEFAULT NULL,
  `alamat_peserta` varchar(100) DEFAULT NULL,
  `no_hp` char(15) DEFAULT NULL,
  `no_reg` char(10) NOT NULL,
  `biaya_reg` int(11) DEFAULT NULL,
  PRIMARY KEY (`no_peserta`),
  KEY `m1` (`no_reg`),
  CONSTRAINT `m1` FOREIGN KEY (`no_reg`) REFERENCES `tbl_registrasi` (`no_reg`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_peserta` */

insert  into `tbl_peserta`(`no_peserta`,`nama_peserta`,`tempat_lahir`,`ttl`,`jk_peserta`,`alamat_peserta`,`no_hp`,`no_reg`,`biaya_reg`) values 
('P-0001','Royani','Jakarta','14 Mei 1989','L','Jl. Diponogor0 Rt.09/001 No. 15 Teluk Betung Barat','0859 2009 0099','DF-0000003',100000),
('P-001','Ningsih','Indramayu','29 Oktober 1995','P','Jalan Raya Bogor no.09','0878 3332 XXXX','DF-0000002',100000);

/*Table structure for table `tbl_registrasi` */

DROP TABLE IF EXISTS `tbl_registrasi`;

CREATE TABLE `tbl_registrasi` (
  `no_reg` char(10) NOT NULL,
  `tgl_reg` date DEFAULT NULL,
  `nama_reg` varchar(50) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(30) DEFAULT NULL,
  `jk_reg` enum('L','P') DEFAULT NULL,
  `alamat_reg` varchar(50) DEFAULT NULL,
  `no_hp` char(15) DEFAULT NULL,
  PRIMARY KEY (`no_reg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_registrasi` */

insert  into `tbl_registrasi`(`no_reg`,`tgl_reg`,`nama_reg`,`tempat_lahir`,`tgl_lahir`,`jk_reg`,`alamat_reg`,`no_hp`) values 
('DF-0000001','2018-08-02','Agus','Bandung','12 Agustus 1987','L','Alamat','0897'),
('DF-0000002','2018-08-02','Ningsih','Indramayu','29 Oktober 1995','P','Jl. antasari no 12 bandar lampung','0819 3345 6132'),
('DF-0000003','2018-08-04','Royani','Jakarta','14 Mei 1989','L','Jl. Ikan Bawal no 12 Teluk betung selatan','0893 4456 212'),
('DF-0000004','2018-08-04','Nama Peserta','Tempat Lahir Peserta','10 Januari 1980','L','Alamat Peserta','No Handphone');

/*Table structure for table `tbl_soal_tes` */

DROP TABLE IF EXISTS `tbl_soal_tes`;

CREATE TABLE `tbl_soal_tes` (
  `no_soal` char(3) NOT NULL,
  `soal` longtext,
  `opt_a` text,
  `opt_b` text,
  `opt_c` text,
  `opt_d` text,
  `kunci` enum('A','B','C','D') DEFAULT NULL,
  PRIMARY KEY (`no_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_soal_tes` */

insert  into `tbl_soal_tes`(`no_soal`,`soal`,`opt_a`,`opt_b`,`opt_c`,`opt_d`,`kunci`) values 
('001','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','<p>1</p>\r\n','A'),
('002','<p>Siapakah Nama Adik Budi ?</p>\r\n','<p>Iwan</p>\r\n','<p>Nama</p>\r\n','<p>Ilham</p>\r\n','<p>Andi</p>\r\n','A'),
('003','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Pimpinan dari Gerakan DI/TII Jawa Tengah, saat itu menjabat sebagai ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di front Tulangan, Sidoarjo, dan Mojokerto</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di front Brebes, Tegal, dan Pekalongan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">&nbsp;Komandan Laskar Hisbullah di front Aceh, Jawa Tengah, dan Jawa Barat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Komandan Laskar Hisbullah di frontBrebes,Sidoarjo, dan Mojokerto</span></p>\r\n','A'),
('004','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Latar belakang terjadinya Pemberontakan Andi Azis adalah</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">tidak menyetujui Indonesia timur bergabung ke NKRI</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">ketidakpuasan daerah terhadap alokasi biaya dan pembangunan dari pusat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">bergabung ke Negara Islam Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">menolak masuknya pasukan APRIS dari TNI ke Sulawesi Selatan</span></p>\r\n','A'),
('005','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Untuk menumpas PRRI, pemerintah dan KSAD memutaskan untuk melancarkan operasi militer. Operasi militer ini diberi nama operasi ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Pagar Betis</span></p>\r\n','<p>Baratayudha</p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Banteng Raiders</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">17 Agustus</span></p>\r\n','D'),
('006','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Munculnya Dewan Banteng, Dewan Gajah, dan Dewan Garuda disebabkan oleh ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">persaingan antara anggota ABRI dan</span><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">&nbsp;perebutan jabatan di Sumatra Barat</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">munculnya kelas-kelas dalam masyarakat di Selawesi Selatan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">perebutan jabatan Kapolres di sebuah kabupaten di Selawesi Selatan</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">ketidakpuasan beberapa di Sumatra dan Sulawesi terhadap dana pembangunan dan pemerintahan pusat</span></p>\r\n','D'),
('007','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">Usaha pemerintah dalam mengatasi Gerakan DI/TII Kartosuwiryo adalah ....</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">pembersihan terhadap antek-antek Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">musyawarah dan pengarahan pasukan TNI</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">penyerangan terhadap Kartosuwiryo</span></p>\r\n','<p><span style=\"font-family:arial,droid sans,sans-serif; font-size:14px\">penangkapan Kartosuwiryo</span></p>\r\n','B'),
('008','<p>15 X 2 = ...</p>\r\n','<p>30</p>\r\n','<p>20</p>\r\n','<p>65</p>\r\n','<p>17</p>\r\n','A');

/*Table structure for table `tbl_thn_ajaran` */

DROP TABLE IF EXISTS `tbl_thn_ajaran`;

CREATE TABLE `tbl_thn_ajaran` (
  `kd_tahun` varchar(20) NOT NULL,
  `keterangan` varchar(20) DEFAULT NULL,
  `tgl_kul` varchar(20) DEFAULT NULL,
  `tgl_awal_perwalian` varchar(20) DEFAULT NULL,
  `tgl_akhir_perwalian` varchar(20) DEFAULT NULL,
  `stts` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`kd_tahun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_thn_ajaran` */

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` varchar(25) NOT NULL,
  `username` varchar(50) NOT NULL,
  `status` enum('admin','mhs','dsn','pim') NOT NULL DEFAULT 'mhs',
  `password` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`username`,`status`,`password`) values 
('adm001','admin','admin','admin'),
('dsn001','Dosen','dsn','dosen'),
('mhs001','mahasiswa','mhs','mhs'),
('pim001','Pimpinan','pim','pim');

/* Trigger structure for table `tbl_mhs` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `af_in_mhs` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `af_in_mhs` AFTER INSERT ON `tbl_mhs` FOR EACH ROW BEGIN
	INSERT INTO tbl_user VALUES (new.nim,new.nama_mhs,'mhs',new.nim);
    END */$$


DELIMITER ;

/*Table structure for table `vw_peserta` */

DROP TABLE IF EXISTS `vw_peserta`;

/*!50001 DROP VIEW IF EXISTS `vw_peserta` */;
/*!50001 DROP TABLE IF EXISTS `vw_peserta` */;

/*!50001 CREATE TABLE  `vw_peserta`(
 `no_peserta` char(10) ,
 `nama_peserta` varchar(50) ,
 `tempat_lahir` varchar(50) ,
 `ttl` varchar(50) ,
 `jk_peserta` enum('L','P') ,
 `alamat_peserta` varchar(100) ,
 `no_hp` char(15) ,
 `no_reg` char(10) ,
 `tgl_reg` date ,
 `biaya_reg` int(11) 
)*/;

/*View structure for view vw_peserta */

/*!50001 DROP TABLE IF EXISTS `vw_peserta` */;
/*!50001 DROP VIEW IF EXISTS `vw_peserta` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_peserta` AS select `tbl_peserta`.`no_peserta` AS `no_peserta`,`tbl_peserta`.`nama_peserta` AS `nama_peserta`,`tbl_peserta`.`tempat_lahir` AS `tempat_lahir`,`tbl_peserta`.`ttl` AS `ttl`,`tbl_peserta`.`jk_peserta` AS `jk_peserta`,`tbl_peserta`.`alamat_peserta` AS `alamat_peserta`,`tbl_peserta`.`no_hp` AS `no_hp`,`tbl_peserta`.`no_reg` AS `no_reg`,`tbl_registrasi`.`tgl_reg` AS `tgl_reg`,`tbl_peserta`.`biaya_reg` AS `biaya_reg` from (`tbl_peserta` join `tbl_registrasi` on((`tbl_peserta`.`no_reg` = `tbl_registrasi`.`no_reg`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
